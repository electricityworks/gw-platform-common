<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="yes" />
    <xsl:param name="root" />
    <xsl:param name="codee-root" />
    <xsl:include href="../CommonXsltTemplates.xslt"/>
    <xsl:param name="exclude-collections" select="'false'" />
    <xsl:param name="relationship-suffix" select="''" />
    <xsl:variable name="airtable" select="/" />
    <xsl:variable name="squot">'</xsl:variable>
    <xsl:variable name="init-space">             </xsl:variable>


    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/">
        <FileSet>
            <FileSetFiles>  
                    <FileSetFile>
                        <xsl:element name="RelativePath"><xsl:text>../../../rabbit_broker_code/rabbitconfig/instance_rabbit_definitions_template.json</xsl:text></xsl:element>
                        <OverwriteMode>Always</OverwriteMode>
                        <xsl:element name="FileContents">
                        <xsl:text>{
    "vhosts":[
        {
            "name":"dev_registry"
        },
        {
            "name":"dw1__1"
        }
    ],
    "permissions":[
        {
            "user":"smqPublic",
            "vhost":"dev_registry",
            "configure":".*",
            "write":".*",
            "read":".*"
        },
        {
            "user":"smqPublic",
            "vhost":"dw1__1",
            "configure":".*",
            "write":".*",
            "read":".*"
        }
    ],
    "exchanges":[
        {
            "name":"worldcoordinatormic",
            "vhost":"dw1__1",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":false,
            "arguments":{}
        },
        {
            "name":"woorldcoordinator",
            "vhost":"dw1__1",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":true,
            "arguments":{}
        }</xsl:text>  
    <xsl:for-each select="$airtable//SassyActors/SassyActor[not(InRegistryBroker='true')]">
    <xsl:text>,
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>mic",
            "vhost":"dw1__1",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":false,
            "arguments":{}
         },
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>",
            "vhost":"dw1__1",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":true,
            "arguments":{}
        }</xsl:text>
    </xsl:for-each> 
    <xsl:for-each select="$airtable//SassyActors/SassyActor[InRegistryBroker='true']">
    <xsl:text>,
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>mic",
            "vhost":"dev_registry",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":false,
            "arguments":{}
         },
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>",
            "vhost":"dev_registry",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":false,
            "arguments":{}
        }</xsl:text>
    </xsl:for-each> 
    <xsl:text>   
    ],
    "bindings":[
    {
        "source":"worldcoordinatormic",
        "vhost":"dw1__1",
        "destination":"admin",
        "destination_type":"exchange",
        "routing_key":"worldcoordinator.*.admin",
        "arguments":{}
    },
    {
        "source":"worldcoordinatormic",
        "vhost":"dw1__1",
        "destination":"supervisor",
        "destination_type":"exchange",
        "routing_key":"worldcoordinator.*.supervisor.#",
        "arguments":{}
    },
    {
        "source":"timecoordinatormic",
        "vhost":"dw1__1",
        "destination":"gnode",
        "destination_type":"exchange",
        "routing_key":"timecoordinator.*.gnode",
        "arguments":{}
    },
    {
        "source":"gnode",
        "vhost":"dw1__1",
        "destination":"terminalasset",
        "destination_type":"exchange",
        "routing_key":"*.*.gnode",
        "arguments":{}
    },
    {
        "source":"gnode",
        "vhost":"dw1__1",
        "destination":"ctn",
        "destination_type":"exchange",
        "routing_key":"*.*.gnode",
        "arguments":{}
    },
    {
        "source":"gnode",
        "vhost":"dw1__1",
        "destination":"networkmodeler",
        "destination_type":"exchange",
        "routing_key":"*.*.gnode",
        "arguments":{}
    },
    {
        "source":"gnode",
        "vhost":"dw1__1",
        "destination":"interconnectioncomponent",
        "destination_type":"exchange",
        "routing_key":"*.*.gnode",
        "arguments":{}
    }</xsl:text>
    <xsl:for-each select="$airtable//SassyPairings/SassyPairing[not(normalize-space(FromActor)='')]">
        <xsl:text>,
    {
        "source":"</xsl:text><xsl:value-of select="translate(FromActor,$ucletters, $lcletters)"/><xsl:text>mic",
        "vhost":"dw1__1",
        "destination":"</xsl:text><xsl:value-of select="translate(ToActor,$ucletters, $lcletters)"/><xsl:text>",
        "destination_type":"exchange",
        "routing_key":"</xsl:text><xsl:value-of select="translate(FromActor,$ucletters, $lcletters)"/>
        <xsl:text>.*.</xsl:text>
        <xsl:value-of select="translate(ToActor,$ucletters, $lcletters)"/>
        <xsl:if test="ToActorIsGNode = 'true'">
            <xsl:text>.#</xsl:text>
        </xsl:if>
        <xsl:text>",
        "arguments":{}
    }</xsl:text>
    </xsl:for-each> 
    <xsl:text>
    ]    
}
</xsl:text>
                    </xsl:element>
                    </FileSetFile>
                    <FileSetFile>
                        <xsl:element name="RelativePath"><xsl:text>../../../rabbit_broker_code/rabbitconfig/instance_rabbit_definitions_debug_template.json</xsl:text></xsl:element>
                        <OverwriteMode>Always</OverwriteMode>
                        <xsl:element name="FileContents">
                        <xsl:text>{
    "vhosts":[
        {
            "name":"dev_registry"
        },
        {
            "name":"dw1__1"
        }
    ],
    "permissions":[
        {
            "user":"smqPublic",
            "vhost":"dev_registry",
            "configure":".*",
            "write":".*",
            "read":".*"
        },
        {
            "user":"smqPublic",
            "vhost":"dw1__1",
            "configure":".*",
            "write":".*",
            "read":".*"
        }
    ],
    "exchanges":[
        {
            "name":"worldcoordinatormic",
            "vhost":"dw1__1",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":false,
            "arguments":{}
        },
        {
            "name":"woorldcoordinator",
            "vhost":"dw1__1",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":true,
            "arguments":{}
        }</xsl:text>  
    <xsl:for-each select="$airtable//SassyActors/SassyActor[not(InRegistryBroker='true')]">
    <xsl:text>,
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>mic",
            "vhost":"dw1__1",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":false,
            "arguments":{}
         },
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>",
            "vhost":"dw1__1",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":true,
            "arguments":{}
        }</xsl:text>
    </xsl:for-each> 
    <xsl:for-each select="$airtable//SassyActors/SassyActor[InRegistryBroker='true']">
    <xsl:text>,
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>mic",
            "vhost":"dev_registry",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":false,
            "arguments":{}
         },
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>",
            "vhost":"dev_registry",
            "type":"topic",
            "durable":true,
            "auto_delete":false,
            "internal":false,
            "arguments":{}
        }</xsl:text>
    </xsl:for-each> 
    <xsl:text>   
    ],
    "queues":[
        {
            "name":"worldcoordinatorlocal.all",
            "vhost":"dw1__1",
            "durable":true,
            "auto_delete":false,
            "arguments":{}
        }</xsl:text>  
<xsl:for-each select="$airtable//SassyActors/SassyActor[not(InRegistryBroker='true')]">
<xsl:text>,
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>.all",
            "vhost":"dw1__1",
            "durable":true,
            "auto_delete":false,
            "arguments":{}
        }</xsl:text>
</xsl:for-each> 
<xsl:for-each select="$airtable//SassyActors/SassyActor[InRegistryBroker='true']">
<xsl:text>,
        {
            "name":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
            <xsl:text>.all",
            "vhost":"dev_registry",
            "durable":true,
            "auto_delete":false,
            "arguments":{}
        }</xsl:text>
</xsl:for-each> 
    <xsl:text>
    ],
    "bindings":[
    {
        "source":"worldcoordinatormic",
        "vhost":"dw1__1",
        "destination":"admin",
        "destination_type":"exchange",
        "routing_key":"worldcoordinator.*.admin",
        "arguments":{}
    },
    {
        "source":"worldcoordinatormic",
        "vhost":"dw1__1",
        "destination":"supervisor",
        "destination_type":"exchange",
        "routing_key":"worldcoordinator.*.supervisor.#",
        "arguments":{}
    },
    {
        "source":"timecoordinatormic",
        "vhost":"dw1__1",
        "destination":"gnode",
        "destination_type":"exchange",
        "routing_key":"timecoordinator.*.gnode",
        "arguments":{}
    },
    {
        "source":"gnode",
        "vhost":"dw1__1",
        "destination":"terminalasset",
        "destination_type":"exchange",
        "routing_key":"*.*.gnode",
        "arguments":{}
    },
    {
        "source":"gnode",
        "vhost":"dw1__1",
        "destination":"ctn",
        "destination_type":"exchange",
        "routing_key":"*.*.gnode",
        "arguments":{}
    },
    {
        "source":"gnode",
        "vhost":"dw1__1",
        "destination":"networkmodeler",
        "destination_type":"exchange",
        "routing_key":"*.*.gnode",
        "arguments":{}
    },
    {
        "source":"gnode",
        "vhost":"dw1__1",
        "destination":"interconnectioncomponent",
        "destination_type":"exchange",
        "routing_key":"*.*.gnode",
        "arguments":{}
    }</xsl:text>
    <xsl:for-each select="$airtable//SassyPairings/SassyPairing[not(normalize-space(FromActor)='')]">
        <xsl:text>,
    {
        "source":"</xsl:text><xsl:value-of select="translate(FromActor,$ucletters, $lcletters)"/><xsl:text>mic",
        "vhost":"dw1__1",
        "destination":"</xsl:text><xsl:value-of select="translate(ToActor,$ucletters, $lcletters)"/><xsl:text>",
        "destination_type":"exchange",
        "routing_key":"</xsl:text><xsl:value-of select="translate(FromActor,$ucletters, $lcletters)"/>
        <xsl:text>.*.</xsl:text>
        <xsl:value-of select="translate(ToActor,$ucletters, $lcletters)"/>
        <xsl:if test="ToActorIsGNode = 'true'">
            <xsl:text>.#</xsl:text>
        </xsl:if>
        <xsl:text>",
        "arguments":{}
    }</xsl:text>
    </xsl:for-each> 
    <xsl:for-each select="$airtable//SassyActors/SassyActor">
    <xsl:text>, 
    {
        "source":"</xsl:text><xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/>
        <xsl:text>",
        "vhost":"</xsl:text>
        <xsl:if test="InRegistryBroker='true'">
            <xsl:text>dev_registry</xsl:text>
        </xsl:if>
        <xsl:if test="not (InRegistryBroker='true')">
            <xsl:text>dw1__1</xsl:text>
        </xsl:if>
        <xsl:text>",
        "destination":"</xsl:text>
        <xsl:value-of select="translate(Alias,$ucletters, $lcletters)"/> <xsl:text>.all",
        "destination_type":"queue",
        "routing_key":"#",
        "arguments":{}
     }</xsl:text>
     </xsl:for-each>
    <xsl:text>
    ]    
}
</xsl:text>
                    </xsl:element>
                    </FileSetFile>
            </FileSetFiles>
        </FileSet>
    </xsl:template>
</xsl:stylesheet>