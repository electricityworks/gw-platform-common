<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="yes" />
    <xsl:param name="root" />
    <xsl:param name="codee-root" />
    <xsl:include href="../CommonXsltTemplates.xslt"/>
    <xsl:param name="exclude-collections" select="'false'" />
    <xsl:param name="relationship-suffix" select="''" />
    <xsl:variable name="airtable" select="/" />
    <xsl:variable name="odxml" select="document('DataSchema.odxml')" />
    <xsl:variable name="squot">'</xsl:variable>
    <xsl:variable name="init-space">             </xsl:variable>


    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/">
        <FileSet>
            <FileSetFiles>
                <xsl:for-each select="$airtable//MpSchemas/MpSchema[(normalize-space(Title) !='') and (FromDataClass = 'true') and (Status = 'ActivelySupported')]">
                    <xsl:variable name="entity" select="." />
                    <xsl:variable name="mp-schema-alias" select="Alias" />  
                    <xsl:variable name="mp-schema-id" select="MpSchemaId" />
                    <xsl:variable name="od" select="$odxml//ObjectDefs/ObjectDef[Name=$entity/Title]"/>

                    <FileSetFile>
                        <xsl:element name="RelativePath"><xsl:text>../../../../schemata/data_classes/</xsl:text><xsl:value-of select="Alias"/><xsl:text>x.yml</xsl:text></xsl:element>
                        <OverwriteMode>Always</OverwriteMode>
                        <xsl:element name="FileContents">
<xsl:text>asyncapi: 2.0.0 WORK IN PROGRESS - SEE DataClassPropertyAttributes table and think through check that the Attributes column
    in that table matches the PropertyDefs.
info:
    title: </xsl:text><xsl:value-of select="Title"/><xsl:text>
    description: |
        Structure of entity </xsl:text><xsl:value-of select="Title"/><xsl:text> of the Gridworks platform, as specified in the 
        gw_platform_django/data_classes/ of the gw-platform-django repo at short git commit </xsl:text><xsl:value-of select="DjangoShortGitHash"/><xsl:text>.
        version: '</xsl:text><xsl:value-of select="SemanticVersion"/><xsl:text>'
    mp_alias: '</xsl:text><xsl:value-of select="Alias"/><xsl:text>'
    contact:
        email: dev@gridworks-consulting.com
    message_passing_mechanism: </xsl:text><xsl:value-of select="MessagePassingMechanism"/><xsl:text>
        message:
            payload:
                type: object
                properties:
        axioms:</xsl:text>
        <xsl:for-each select = "$airtable//MpSchemaAxioms/MpSchemaAxiom[MpSchemaId = $mp-schema-id]">
        <xsl:text>
            - </xsl:text> <xsl:value-of select="AxiomAlias"/>
        </xsl:for-each>

        <xsl:for-each select = "$airtable//MpMessagePayloadProperties/MpMessagePayloadProperty[(MpSchema = $mp-schema-id) and (IsCoreLexiconTerm= 'true')]">
            <xsl:variable name="current-property" select="Value"/>
            <xsl:for-each select = "$airtable//PropertyTriggeredAxioms/PropertyTriggeredAxiom"><xsl:if test="$current-property = PropertyValue">
        <xsl:text> 
            - </xsl:text><xsl:value-of select="AxiomAlias"/>
            </xsl:if></xsl:for-each>
        </xsl:for-each>

</xsl:element>
</FileSetFile>
                </xsl:for-each>

            </FileSetFiles>
        </FileSet>
    </xsl:template>

    <xsl:template name="wrap-text">
        <xsl:param name="text"></xsl:param>
        <xsl:param name="index" select="1" />
        <xsl:variable name="next-char" select="substring($text, 1, 1)" />
        <xsl:value-of select="$next-char"/>
        <xsl:choose>
            <xsl:when test="$next-char = ' ' and $index > 70" xml:space="preserve">
<xsl:if test="string-length($text) > 1" xml:space="default">
            <xsl:call-template name="wrap-text">
                <xsl:with-param name="text" select="substring($text, 2, string-length($text))" />
                <xsl:with-param name="index" select="1" />
            </xsl:call-template></xsl:if></xsl:when>
            <xsl:otherwise>
                <xsl:if test="string-length($text) > 1">
                    <xsl:call-template name="wrap-text">
                        <xsl:with-param name="text" select="substring($text, 2, string-length($text))" />
                        <xsl:with-param name="index" select="$index + 1" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
</xsl:stylesheet>