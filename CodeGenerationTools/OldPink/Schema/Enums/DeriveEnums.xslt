<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="yes" />
    <xsl:param name="root" />
    <xsl:param name="codee-root" />
    <xsl:include href="../CommonXsltTemplates.xslt"/>
    <xsl:param name="exclude-collections" select="'false'" />
    <xsl:param name="relationship-suffix" select="''" />
    <xsl:variable name="airtable" select="/" />
    <xsl:variable name="squot">'</xsl:variable>
    <xsl:variable name="init-space">             </xsl:variable>


    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/">
        <FileSet>
            <FileSetFiles>
                <xsl:for-each select="$airtable//GwFinStringSets/GwFinStringSet">
                    <xsl:variable name="finite-string-set-alias" select="Alias" />  
                    <xsl:variable name="finite-string-set-id" select="GwFinStringSetId" />
                

                    <FileSetFile>
                        <xsl:element name="RelativePath"><xsl:text>../../../../enums/</xsl:text><xsl:value-of select="Alias"/><xsl:text>.yml</xsl:text></xsl:element>
                        <OverwriteMode>Always</OverwriteMode>
                        <xsl:element name="FileContents">
<xsl:text>asyncapi: 2.0.0
    info:
        finite_string_set_alias: '</xsl:text><xsl:value-of select="Alias"/><xsl:text>'
        values: 
        </xsl:text>
          <xsl:for-each select="$airtable//GwFinStringSetMembers/GwFinStringSetMember[(GwFinStringSet = $finite-string-set-id) and normalize-space(Value) != '']">           
        <xsl:text>  - '</xsl:text><xsl:value-of select="Value"/><xsl:text>'
        </xsl:text>
            </xsl:for-each>
        
        <xsl:text>local_context_name: '</xsl:text><xsl:value-of select="LocalContextName"/><xsl:text>'
        </xsl:text>
</xsl:element>
                    </FileSetFile>
                    <FileSetFile>
                        <xsl:element name="RelativePath"><xsl:text>../../../GwPlatformCommon/MpSchemas/FiniteStringSets/Descriptions/</xsl:text><xsl:value-of select="Alias"/><xsl:text>.Descriptions.yml</xsl:text></xsl:element>
                        <OverwriteMode>Always</OverwriteMode>
                        <xsl:element name="FileContents">
<xsl:text>asyncapi: 2.0.0
    info:
        finite_string_set_alias: </xsl:text><xsl:value-of select="Alias"/><xsl:text> 
        description: |
            </xsl:text><xsl:call-template name="wrap-text">
            <xsl:with-param name="text" select="Description"/>
        </xsl:call-template>
        <xsl:text>
        choice_descriptions:
            </xsl:text>
        <xsl:for-each select="$airtable//GwFinStringSetMembers/GwFinStringSetMember[(GwFinStringSet = $finite-string-set-id) and normalize-space(Value) != '']">
        <xsl:value-of select="Value"/><xsl:text>: |
            </xsl:text>
            <xsl:call-template name="wrap-text">
                <xsl:with-param name="text" select="Definition"/>
            </xsl:call-template><xsl:text> ~
            </xsl:text>
        </xsl:for-each>
</xsl:element>
                    </FileSetFile>
                </xsl:for-each>

            </FileSetFiles>
        </FileSet>
    </xsl:template>

    <xsl:template name="wrap-text">
        <xsl:param name="text"></xsl:param>
        <xsl:param name="index" select="1" />
        <xsl:variable name="next-char" select="substring($text, 1, 1)" />
        <xsl:value-of select="$next-char"/>
        <xsl:choose>
            <xsl:when test="$next-char = ' ' and $index > 70" xml:space="preserve">
<xsl:if test="string-length($text) > 1" xml:space="default">
            <xsl:call-template name="wrap-text">
                <xsl:with-param name="text" select="substring($text, 2, string-length($text))" />
                <xsl:with-param name="index" select="1" />
            </xsl:call-template></xsl:if></xsl:when>
            <xsl:otherwise>
                <xsl:if test="string-length($text) > 1">
                    <xsl:call-template name="wrap-text">
                        <xsl:with-param name="text" select="substring($text, 2, string-length($text))" />
                        <xsl:with-param name="index" select="$index + 1" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
</xsl:stylesheet>