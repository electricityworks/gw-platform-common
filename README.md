# gw-platform-common
This repo is the authority for how to create rabbit brokers for GridWorks environments. 

Note that each world (and there can be many shadow, dev and hybrid worlds) will have its own world rabbit broker. The pattern is all very similar - which is why there is a `world_rabbit_definitions_template.json` - but the name of the vhost will encode the world.

This repo is also where we build the docker image for dev rabbit brokers.

There is some old stuff here from when this repo was supposed to support a schema registry for the gridworks schemata.


# Recipe for making a new hybrid world rabbit broker for world instance hw1__1

TODO - writeup for
  - launch from world broker template on ec2 using key gridworks-hybrid.pem
  - assign to fqdn hw1-1.electricity.works on route53

Once the above is accessible:

ssh -A ubuntu@hw1-1.electricity.works

git clone https://jessmillar@bitbucket.org/electricityworks/gw-platform-common.git

cd gw-platform-common/rabbit_broker_code

nano world_secret_vars.sh


#!/bin/bash
WORLD_RABBIT_PASSWORD="ADD" [ASK JESSICA FOR WHAT THIS IS]


./ubuntu-world-broker-setup.sh hybrid hw1__1

sudo docker-compose -f world_rmq.yml up -d