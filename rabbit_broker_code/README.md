## Dev simulated world instance ##

    docker-compose -f dev-rmq.yml up -d

This will bring up  a rabbit broker with dw1__1 as a vhost and also dev_registry as a vhost. This is the simplest way to develop in other repos.
Test for success: after the rabbit finishes coming up, go here
http://0.0.0.0:15672/
and enter smqPublic, smqPublic.  The exchanges should have both dw1__1 and dev_registry vhosts.


## Dev registries running via docker-compose ##

If you are testing out the construction of registries and worlds, you will want to read this section.
### Intro ###
The following will set up local git ignored data for your g node registry services within the `container_data/dev/registry` subfolders. The structure is almost identical to the shadow registries, except for the mount point of the postgres databases, the values for the config variables and the values for the secret variables. 
  * **Mount points** For the shadow registries, these mount points are EBS volume mounted in the root directory of an EC2 instance. Here the point to logical (gitignored) subdirectories of the container_data folder.
  * **Config variables** The config variables for both dev and shadow are organized by universe, world and container and stored in this repo within the `container_data` folder structure. 
  * **Secret variables** For dev these are in the container_data folder structure. For shadow they are stored in bitbucket as secrets, and these are accessed by the  [gw-platform-python](https://bitbucket.org/electricityworks/gw-platform-python/src/master/)  pipeline.

### Set-up Directions ###
From this directory (gw-platform-common/docker):

    cd docker
    touch container_data/dev/registry/component_registry/default_cr_db.sqlite3
    touch container_data/dev/registry/g_node_registry/default_gnr_db.sqlite3
    touch container_data/dev/registry/world_instance_registry/default_wir_db.sqlite3
    touch container_data/dev/worlds/dw1__1/world_coordinator_registry/default_wcr_db.sqlite3


    ./mac-registry-setup.sh
    
    This script will create a bunch of settings files inside the DjangoConfig subfolder.

(If you run the docker-compose files below BEFORE this step, docker will create FOLDERS for these sqlite3 files
and you will need to delete these folders and touch the files with the same name before the registries will 
work correctly.)

You can now start the dev registries in docker containers by FIRST starting dev-rmq as above, and then running:

    docker-compose -f dev-registry.yml up -d

Likewise you can run a dockerized version of world coordinator for dw1__1 by running

    docker-compose -f dev-world-coord.yml up -d


The dev registries (ComponentRegistry, GNodeRegistry, WorldInstanceRegistry) can all communicate with each other
and also with the WorldCoordinator for world instance dw1__1 through the dev_registry vhost, which is on the `rmq` host
on the dev network (as created by the dev-rmq.yml).


Verify that containers dev-cr, dev-gnr, dev-wir and their 3 postgres database containers are up and running. This will mount the above default sqlite3 files and also subfolders for postgres databases in each of the dev registry folders above. 

Verify that these new files and folders exist and are ignored by git.

Right now we are still running migrations by hand and also adding the admin user to log into the website by hand. Do this:

    docker exec -it dev-cr /bin/bash
    python manage.py migrate
    python manage.py migrate --database=cr_db


    echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@elfhat.io', 'admin')" | python3 manage.py shell

Verify http://0.0.0.0:8000/admin/ can log in with admin/admin

Verify with `git status` that your additional data within `container_data/dev/registry/component_registry` is not showing up on git.

    docker exec -it dev-gnr /bin/bash
    python manage.py migrate
    python manage.py migrate --database=gnr_db


    echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@elfhat.io', 'admin')" | python3 manage.py shell

Verify http://0.0.0.0:8000/admin/ can log in with admin/admin

Verify with `git status` that your additional data within `container_data/dev/registry/g_node_registry` is not showing up on git.

    docker exec -it dev-wir /bin/bash

    python manage.py migrate
    python manage.py migrate --database=wir_db


    echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@elfhat.io', 'admin')" | python3 manage.py shell

verify http://0.0.0.0:8002/admin/ can log in with admin/admin

Verify with `git status` that your additional data within `container_data/dev/registry/world_instance_registry` is not showing up on git.


## Regular Instructions ##

Change the code in [gw-platform-python](https://bitbucket.org/electricityworks/gw-platform-python/src/master/) and follow instructions there for creating `latest` docker images for the registries. 

Then:

    docker-compose -f dev_registry.yml up -d

If this complains that dev_registry.yml does not exist, go back and follow the set-up instructions above.

**If you have created new migrations you need to apply them by hand in order for them to be reflected in the database**

**ComponentRegistry:**
   
    docker exec -it dev-cr /bin/bash
    python manage.py migrate
    python manage.py migrate --database=cr_db

**GNodeRegistry:**
   
    docker exec -it dev-gnr /bin/bash
    python manage.py migrate
    python manage.py migrate --database=gnr_db

**WirRegistry:**
   
    docker exec -it dev-wir /bin/bash
    python manage.py migrate
    python manage.py migrate --database=wir_db