#!/bin/bash
source universe_secret_vars.sh

echo [default] > aws_configuration/credentials
echo aws_access_key_id = $AWS_ACCESS_KEY_ID >> aws_configuration/credentials
echo aws_secret_access_key = $AWS_SECRET_ACCESS_KEY >> aws_configuration/credentials