#!/bin/bash
source universe_config_vars.sh
source universe_secret_vars.sh
source world_config_vars.sh
source world_secret_vars.sh


echo LOCAL_WORLD_AMQP_URL = \'amqp://$WORLD_RABBIT_USER:$WORLD_RABBIT_PASSWORD@$WORLD_RABBIT_FQDN:5672/$WORLD_RABBIT_VHOST\' > rabbitconfig/privkeys.py
echo REGISTRY_AMQP_URL = \'amqp://$REGISTRY_RABBIT_USER:$REGISTRY_RABBIT_PASSWORD@$REGISTRY_RABBIT_FQDN:5672/$REGISTRY_RABBIT_VHOST\' >> rabbitconfig/privkeys.py

echo RMQ_LOCAL_WORLD_SSL = False >> rabbitconfig/privkeys.py
echo RMQ_REGISTRY_SSL = False >> rabbitconfig/privkeys.py
