#!/bin/bash
source universe_config_vars.sh

REGISTRY_PYTHON_FOLDER_NAME='world_coordinator_registry'
REGISTRY_DB_ROUTER='world_coordinator_registry.WcrTestDbRouter'
DEFAULT_SQLITE3_DB_NAME='default_wcr_db.sqlite3'
REGISTRY_DJANGO_DB_NAME='wcr_db'
REGISTRY_URL_TAG='world_coordinator_registry'
REGISTRY_SMALLEST_TAG='wcr'
REGISTRY_DB_NAME='dw1_1_wcr_db'
REGISTRY_DB_USER='dw1_1_wcr_db'
REGISTRY_DB_HOST='dw1-1-wcr-db'
