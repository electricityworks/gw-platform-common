#!/bin/bash
source universe_config_vars.sh

REGISTRY_PYTHON_FOLDER_NAME='world_coordinator_registry'
REGISTRY_DB_ROUTER='world_coordinator_registry.db_settings.WcrTestDbRouter'
DEFAULT_SQLITE3_DB_NAME='default_wcr_db.sqlite3'
REGISTRY_DJANGO_DB_NAME='wcr_db'
REGISTRY_URL_TAG='world_coordinator_registry'
REGISTRY_SMALLEST_TAG='wcr'
DATA_FOLDER='\.\/container_data\/dev\/worlds\/dw1__2\/world_coordinator_registry\/'
DEFAULT_SQLITE3_DB_PATH="$DATA_FOLDER$DEFAULT_SQLITE3_DB_NAME"
DB_ROOT_MOUNT_FOLDER="$DATA_FOLDER"dw1__2_data
REGISTRY_DB_NAME='dw1_2_wcr_db'
REGISTRY_DB_USER='dw1_2_wcr_db'
REGISTRY_DB_HOST='dw1-2-wcr-db'
