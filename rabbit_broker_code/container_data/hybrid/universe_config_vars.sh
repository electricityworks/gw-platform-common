#!/bin/bash
UNIVERSE_DOMAIN_NAME='electricity.works'
REGISTRY_RABBIT_FIRST_DOMAIN_NAME_TERM='sgwps'
REGISTRY_RABBIT_FQDN="$REGISTRY_RABBIT_FIRST_DOMAIN_NAME_TERM.$UNIVERSE_DOMAIN_NAME"
REGISTRY_RABBIT_VHOST='shadow_registry'
REGISTRY_RABBIT_USER='smqPublic'