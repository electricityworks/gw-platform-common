#!/bin/bash
source universe_config_vars.sh
WORLD_TYPE='instance'
WORLD_RABBIT_USER='smqPublic'
WORLD_RABBIT_FIRST_DOMAIN_NAME_TERM='hw1-1'
WORLD_RABBIT_FQDN="hw1-1.gridworks-consulting.com"
WORLD_RABBIT_VHOST="hw1__1"

WCR_REGISTRY_PYTHON_FOLDER_NAME='world_coordinator_registry'
WCR_REGISTRY_DB_ROUTER='world_coordinator_registry.db_settings.WcrTestDbRouter'
WCR_DATA_FOLDER='\.\/container_data\/hybrid\/worlds\/hw1__1\/world_coordinator_registry\/'
WCR_DEFAULT_SQLITE3_DB_NAME='default_wcr_db.sqlite3'
WCR_DEFAULT_SQLITE3_DB_PATH="$WCR_DATA_FOLDER$WIR_DEFAULT_SQLITE3_DB_NAME"
WCR_REGISTRY_URL_TAG='wcr'
WCR_REGISTRY_DJANGO_DB_NAME='wcr_db'
WCR_REGISTRY_SMALLEST_TAG='wcr'
WCR_LOCAL_DB_FOLDER='dev_wcr_data'
WCR_DB_ROOT_MOUNT_FOLDER="$WCR_DATA_FOLDER$WCR_LOCAL_DB_FOLDER"
