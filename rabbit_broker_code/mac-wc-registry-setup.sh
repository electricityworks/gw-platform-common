#!/bin/bash

if [ $# -eq 0 ]; then
    UNIVERSE='dev'
    WORLD_ALIAS='dw1__1'
    echo "bringing up dev world coorldinator registry for dw1__1"
elif [ $# -eq 1 ]; then
    UNIVERSE='dev'
    echo "bringing up dev world coorldinator registry for dw1__1"
elif [ "$1" == 'shadow' ]; then
    UNIVERSE='shadow'
    WORLD_ALIAS=$2
    echo "bringing up shadow registry for $WORLD_ALIAS"
else
    echo "do not recognize $1 as a universe. exiting"
    exit 1
fi

if [ "$UNIVERSE" == 'dev' ]; then
    cp container_data/dev/universe_secret_vars.sh universe_secret_vars.sh
    cp container_data/dev/registry/world_secret_vars.sh world_secret_vars.sh
    cp container_data/dev/worlds/"$WORLD_ALIAS"/world_coordinator_registry/container_secret_vars.sh container_secret_vars.sh
fi

STOP_RUN=false
if [ ! -f universe_secret_vars.sh ]; then
    echo "please make sure this directory contains universe_secret_vars.sh"
    STOP_RUN=true
fi
if [ ! -f world_secret_vars.sh ]; then
    echo "please make sure this directory contains world_secret_vars.sh"
    STOP_RUN=true
fi
if [ ! -f container_secret_vars.sh ]; then
    echo "please make sure this directory contains container_secret_vars.sh for world coordinator"
    STOP_RUN=true
fi
if [ "$STOP_RUN" == 'true' ]; then
    echo "Exiting!"
    exit 1
fi

cp mac_os/build_django_settings.sh build_django_settings.sh
cp mac_os/build_django_urls.sh build_django_urls.sh

cp container_data/"$UNIVERSE"/universe_config_vars.sh universe_config_vars.sh
cp container_data/"$UNIVERSE"/registry/world_config_vars.sh world_config_vars.sh
cp container_data/"$UNIVERSE"/worlds/"$WORLD_ALIAS"/world_coordinator_registry/container_config_vars.sh container_config_vars.sh


source universe_config_vars.sh
source universe_secret_vars.sh
source world_config_vars.sh
source world_secret_vars.sh
source container_config_vars.sh
source container_secret_vars.sh



cp wc_registry_template.yml wc_registry.yml
sed -i "" "s/UNIVERSE_SHORT_TAG_PLACEHOLDER/$UNIVERSE_SHORT_TAG/g" wc_registry.yml
sed -i "" "s/REGISTRY_DATA_ROOT_PLACEHOLDER/$REGISTRY_DATA_ROOT/g" wc_registry.yml
sed -i "" "s/REGISTRY_DB_HOST_PLACEHOLDER/$REGISTRY_DB_HOST/g" wc_registry.yml
sed -i "" "s/DEFAULT_SQLITE3_DB_NAME_PLACEHOLDER/$DEFAULT_SQLITE3_DB_NAME/g" wc_registry.yml
sed -i "" "s/DEFAULT_SQLITE3_DB_PATH_PLACEHOLDER/$DEFAULT_SQLITE3_DB_PATH/g" wc_registry.yml
sed -i "" "s/REGISTRY_DB_NAME_PLACEHOLDER/$REGISTRY_DB_NAME/g" wc_registry.yml
sed -i "" "s/REGISTRY_DB_USER_PLACEHOLDER/$REGISTRY_DB_USER/g" wc_registry.yml
sed -i "" "s/REGISTRY_DB_PASSWORD_PLACEHOLDER/$REGISTRY_DB_PASSWORD/g" wc_registry.yml
sed -i "" "s/DB_ROOT_MOUNT_FOLDER_PLACEHOLDER/$DB_ROOT_MOUNT_FOLDER/g" wc_registry.yml


./build_privkeys.sh
./build_aws_credentials.sh
./build_django_settings.sh
./build_django_urls.sh
./build_aws_config_py.sh

rm build_django_settings.sh
rm build_django_urls.sh