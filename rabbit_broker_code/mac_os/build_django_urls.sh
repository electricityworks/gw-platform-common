#!/bin/bash
# Run this for django based Registry UI applications
source universe_config_vars.sh
source world_config_vars.sh
source world_secret_vars.sh
source container_config_vars.sh
source container_secret_vars.sh

SETTINGS_FILE=DjangoConfig/"$REGISTRY_SMALLEST_TAG"_django_urls.py
echo Building django_urls $SETTINGS_FILE

cp DjangoConfig/django_urls_template.py $SETTINGS_FILE
sed -i "" "s/REGISTRY_URL_TAG_PLACEHOLDER/$REGISTRY_URL_TAG/g"   $SETTINGS_FILE
sed -i "" "s/REGISTRY_PYTHON_FOLDER_NAME_PLACEHOLDER/$REGISTRY_PYTHON_FOLDER_NAME/g"   $SETTINGS_FILE

#rm  $SETTINGS_FILE

