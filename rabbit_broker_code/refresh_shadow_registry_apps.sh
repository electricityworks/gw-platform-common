
#!/bin/bash
sudo docker-compose -f registry.yml down
sudo docker pull jessmillar/world-instance-registry
sudo docker pull jessmillar/g-node-registry
sudo docker pull jessmillar/component-registry
sudo docker rmi $(sudo docker images | grep "<none>" | awk '{print $3}') 2>/dev/null || echo "No untagged images to delete."
sudo docker-compose -f registry.yml up -d