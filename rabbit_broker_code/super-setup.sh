
#!/bin/bash

if [ $# -eq 0 ]; then
    UNIVERSE='dev'
    WORLD_ID='dw1__1'
    echo "creating super.yml for dw1__1"
elif [ $# -eq 1 ]; then
    UNIVERSE='dev'
    WORLD_ID='dw1__1'
    echo "creating super.yml for  dw1__1"
elif [ "$1" == 'dev' ]; then
    UNIVERSE='dev'
    WORLD_ID=$2
    echo "creating super.yml for $2"
elif [ "$1" == 'shadow' ]; then
    UNIVERSE='shadow'
    WORLD_ID=$2
    echo "creating super.yml for $2"
else
    echo "do not recognize $1 as a universe. exiting"
    exit 1
fi

if [ ! -f world_secret_vars.sh ]; then
    echo "please make sure this directory contains world_secret_vars.sh"
    echo "Exiting!"
    exit 1
fi

cp container_data/"$UNIVERSE"/worlds/"$WORLD_ID"/world_config_vars.sh world_config_vars.sh
cp container_data/"$UNIVERSE"/universe_config_vars.sh universe_config_vars.sh

source world_config_vars.sh
source world_secret_vars.sh

echo LOCAL_WORLD_AMQP_URL = \'amqp://$WORLD_RABBIT_USER:$WORLD_RABBIT_PASSWORD@$WORLD_RABBIT_FQDN:5672/$WORLD_RABBIT_VHOST\' > rabbitconfig/privkeys.py
echo RMQ_LOCAL_WORLD_SSL = False >> rabbitconfig/privkeys.py

