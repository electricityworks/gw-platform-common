#!/bin/bash

if [ $# -eq 0 ]; then
    UNIVERSE='dev'
    echo "bringing up dev registry broker"
elif [ "$1" == 'dev' ]; then
    UNIVERSE='dev'
    echo "bringing up dev registry broker"
elif [ "$1" == 'shadow' ]; then
    UNIVERSE='shadow'
    echo "bringing up shadow registry broker"
else
    echo "do not recognize $1 as a universe. exiting"
    exit 1
fi

if [ "$UNIVERSE" == 'dev' ]; then
    cp container_data/dev/registry/world_secret_vars.sh world_secret_vars.sh
fi


STOP_RUN=false
if [ ! -f world_secret_vars.sh ]; then
    echo "please make sure this directory contains world_secret_vars.sh"
    STOP_RUN=true
fi
if [ "$STOP_RUN" == 'true' ]; then
    echo "Exiting!"
    exit 1
fi

cp ubuntu_os/build_rabbitmq_conf.sh build_rabbitmq_conf.sh
cp ubuntu_os/build_rabbit_definitions_json.sh build_rabbit_definitions_json.sh

cp container_data/"$UNIVERSE"/universe_config_vars.sh universe_config_vars.sh
cp container_data/"$UNIVERSE"/registry/world_config_vars.sh world_config_vars.sh

source universe_config_vars.sh
source world_config_vars.sh
source world_secret_vars.sh

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
        # linux. OFI: find bash method of determining arm vs x86 architecture
    cp rabbit_broker_arm_template.yml rabbit_broker.yml
elif [[ "$OSTYPE" == "darwin"* ]]; then
        # Mac OSX
    cp rabbit_broker_x86_template.yml rabbit_broker.yml
fi

sed -i  "s/UNIVERSE_SHORT_TAG_PLACEHOLDER/$UNIVERSE_SHORT_TAG/g"  rabbit_broker.yml
sed -i  "s/WORLD_RABBIT_FIRST_DOMAIN_NAME_TERM_PLACEHOLDER/$WORLD_RABBIT_FIRST_DOMAIN_NAME_TERM/g"  rabbit_broker.yml
sed -i  "s/WORLD_TYPE_PLACEHOLDER/$WORLD_TYPE/g"  rabbit_broker.yml
sed -i  "s/RABBIT_USER_PLACEHOLDER/$WORLD_RABBIT_USER/g" rabbit_broker.yml
sed -i  "s/RABBIT_USER_PASSWORD_PLACEHOLDER/$WORLD_RABBIT_PASSWORD/g"  rabbit_broker.yml

./build_rabbitmq_conf.sh
./build_vhost_env.sh
./build_rabbit_definitions_json.sh

rm build_rabbitmq_conf.sh
rm build_rabbit_definitions_json.sh