#!/bin/bash

if [ $# -eq 0 ]; then
    UNIVERSE='dev'
    echo "bringing up dev registry"
elif [ "$1" == 'dev' ]; then
    UNIVERSE='dev'
    echo "bringing up dev registry"
elif [ "$1" == 'shadow' ]; then
    UNIVERSE='shadow'
    echo "bringing up shadow registry"
else
    echo "do not recognize $1 as a universe. exiting"
    exit 1
fi

if [ "$UNIVERSE" == 'dev' ]; then
    cp container_data/dev/universe_secret_vars.sh universe_secret_vars.sh
    cp container_data/dev/registry/world_secret_vars.sh world_secret_vars.sh
    cp container_data/dev/registry/component_registry/container_secret_vars.sh cr_container_secret_vars.sh
    cp container_data/dev/registry/g_node_registry/container_secret_vars.sh gnr_container_secret_vars.sh
    cp container_data/dev/registry/world_instance_registry/container_secret_vars.sh wir_container_secret_vars.sh
fi

STOP_RUN=false
if [ ! -f universe_secret_vars.sh ]; then
    echo "please make sure this directory contains universe_secret_vars.sh"
    STOP_RUN=true
fi
if [ ! -f world_secret_vars.sh ]; then
    echo "please make sure this directory contains world_secret_vars.sh"
    STOP_RUN=true
fi
if [ ! -f cr_container_secret_vars.sh ]; then
    echo "please make sure this directory contains cr_container_secret_vars.sh"
    STOP_RUN=true
fi
if [ ! -f gnr_container_secret_vars.sh ]; then
    echo "please make sure this directory contains gnr_container_secret_vars.sh"
    STOP_RUN=true
fi
if [ ! -f wir_container_secret_vars.sh ]; then
    echo "please make sure this directory contains wir_container_secret_vars.sh"
    STOP_RUN=true
fi
if [ "$STOP_RUN" == 'true' ]; then
    echo "Exiting!"
    exit 1
fi

cp ubuntu_os/build_django_settings.sh build_django_settings.sh
cp ubuntu_os/build_django_urls.sh build_django_urls.sh

cp container_data/"$UNIVERSE"/universe_config_vars.sh universe_config_vars.sh
cp container_data/"$UNIVERSE"/registry/world_config_vars.sh world_config_vars.sh
cp container_data/"$UNIVERSE"/registry/component_registry/container_config_vars.sh cr_container_config_vars.sh
cp container_data/"$UNIVERSE"/registry/g_node_registry/container_config_vars.sh gnr_container_config_vars.sh
cp container_data/"$UNIVERSE"/registry/world_instance_registry/container_config_vars.sh wir_container_config_vars.sh

source universe_config_vars.sh
source universe_secret_vars.sh
source world_config_vars.sh
source world_secret_vars.sh

./build_aws_credentials.sh
cp registry_template.yml registry.yml
sed -i  "s/UNIVERSE_SHORT_TAG_PLACEHOLDER/$UNIVERSE_SHORT_TAG/g" registry.yml
sed -i  "s/REGISTRY_DATA_ROOT_PLACEHOLDER/$REGISTRY_DATA_ROOT/g" registry.yml
sed -i  "s/CR_REGISTRY_DB_HOST_PLACEHOLDER/$CR_REGISTRY_DB_HOST/g" registry.yml
sed -i  "s/CR_DEFAULT_SQLITE3_DB_NAME_PLACEHOLDER/$CR_DEFAULT_SQLITE3_DB_NAME/g" registry.yml
sed -i  "s/CR_DEFAULT_SQLITE3_DB_PATH_PLACEHOLDER/$CR_DEFAULT_SQLITE3_DB_PATH/g" registry.yml
sed -i  "s/CR_REGISTRY_DB_NAME_PLACEHOLDER/$CR_REGISTRY_DB_NAME/g" registry.yml
sed -i  "s/CR_REGISTRY_DB_USER_PLACEHOLDER/$CR_REGISTRY_DB_USER/g" registry.yml
sed -i  "s/CR_REGISTRY_DB_PASSWORD_PLACEHOLDER/$CR_REGISTRY_DB_PASSWORD/g" registry.yml
sed -i  "s/CR_DB_ROOT_MOUNT_FOLDER_PLACEHOLDER/$CR_DB_ROOT_MOUNT_FOLDER/g" registry.yml

sed -i  "s/GNR_REGISTRY_DB_HOST_PLACEHOLDER/$GNR_REGISTRY_DB_HOST/g" registry.yml
sed -i  "s/GNR_DEFAULT_SQLITE3_DB_NAME_PLACEHOLDER/$GNR_DEFAULT_SQLITE3_DB_NAME/g" registry.yml
sed -i  "s/GNR_DEFAULT_SQLITE3_DB_PATH_PLACEHOLDER/$GNR_DEFAULT_SQLITE3_DB_PATH/g" registry.yml
sed -i  "s/GNR_REGISTRY_DB_NAME_PLACEHOLDER/$GNR_REGISTRY_DB_NAME/g" registry.yml
sed -i  "s/GNR_REGISTRY_DB_USER_PLACEHOLDER/$GNR_REGISTRY_DB_USER/g" registry.yml
sed -i  "s/GNR_REGISTRY_DB_PASSWORD_PLACEHOLDER/$GNR_REGISTRY_DB_PASSWORD/g" registry.yml
sed -i  "s/GNR_DB_ROOT_MOUNT_FOLDER_PLACEHOLDER/$GNR_DB_ROOT_MOUNT_FOLDER/g" registry.yml

sed -i  "s/WIR_REGISTRY_DB_HOST_PLACEHOLDER/$WIR_REGISTRY_DB_HOST/g" registry.yml
sed -i  "s/WIR_DEFAULT_SQLITE3_DB_NAME_PLACEHOLDER/$WIR_DEFAULT_SQLITE3_DB_NAME/g" registry.yml
sed -i  "s/WIR_DEFAULT_SQLITE3_DB_PATH_PLACEHOLDER/$WIR_DEFAULT_SQLITE3_DB_PATH/g" registry.yml
sed -i  "s/WIR_REGISTRY_DB_NAME_PLACEHOLDER/$WIR_REGISTRY_DB_NAME/g" registry.yml
sed -i  "s/WIR_REGISTRY_DB_USER_PLACEHOLDER/$WIR_REGISTRY_DB_USER/g" registry.yml
sed -i  "s/WIR_REGISTRY_DB_PASSWORD_PLACEHOLDER/$WIR_REGISTRY_DB_PASSWORD/g" registry.yml
sed -i  "s/WIR_DB_ROOT_MOUNT_FOLDER_PLACEHOLDER/$WIR_DB_ROOT_MOUNT_FOLDER/g" registry.yml

./build_privkeys.sh

mv cr_container_config_vars.sh container_config_vars.sh
mv cr_container_secret_vars.sh container_secret_vars.sh
source container_config_vars.sh
source container_secret_vars.sh

./build_django_settings.sh
./build_django_urls.sh

mv gnr_container_config_vars.sh container_config_vars.sh
mv gnr_container_secret_vars.sh container_secret_vars.sh
source container_config_vars.sh
source container_secret_vars.sh

./build_django_settings.sh
./build_django_urls.sh

mv wir_container_config_vars.sh container_config_vars.sh
mv wir_container_secret_vars.sh container_secret_vars.sh
source container_config_vars.sh
source container_secret_vars.sh

./build_aws_config_py.sh
./build_django_settings.sh
./build_django_urls.sh


rm container_config_vars.sh
rm container_secret_vars.sh
rm build_django_settings.sh
rm build_django_urls.sh