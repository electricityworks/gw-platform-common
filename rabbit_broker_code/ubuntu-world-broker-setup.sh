#!/bin/bash

if [ $# -eq 0 ]; then
    UNIVERSE='dev'
    WORLD_ID='dw1__1'
    echo "bringing up dev world broker for dw1__1"
elif [ $# -eq 1 ]; then
    UNIVERSE='dev'
    WORLD_ID='dw1__1'
    echo "bringing up dev world broker for dw1__1"
elif [ "$1" == 'dev' ]; then
    UNIVERSE='dev'
    WORLD_ID=$2
    echo "bringing up dev world broker for $2"
elif [ "$1" == 'shadow' ]; then
    UNIVERSE='shadow'
    WORLD_ID=$2
    echo "bringing up a shadow world broker for $2"
elif [ "$1" == 'hybrid' ]; then
    UNIVERSE='hybrid'
    WORLD_ID=$2
    echo "bringing up a hybrid world broker for $2"
else
    echo "do not recognize $1 as a universe. exiting"
    exit 1
fi

if [ "$UNIVERSE" == 'dev' ]; then
    cp container_data/dev/worlds/"$WORLD_ID"/world_secret_vars.sh world_secret_vars.sh
fi

echo $UNIVERSE

STOP_RUN=false
if [ ! -f world_secret_vars.sh ]; then
    echo "please make sure this directory contains world_secret_vars.sh"
    STOP_RUN=true
fi
if [ "$STOP_RUN" == 'true' ]; then
    echo "Exiting!"
    exit 1
fi

cp ubuntu_os/build_rabbitmq_conf.sh build_rabbitmq_conf.sh
cp ubuntu_os/build_rabbit_definitions_json.sh build_rabbit_definitions_json.sh

cp container_data/"$UNIVERSE"/universe_config_vars.sh universe_config_vars.sh
cp container_data/"$UNIVERSE"/worlds/"$WORLD_ID"/world_config_vars.sh world_config_vars.sh

source universe_config_vars.sh
source world_config_vars.sh
source world_secret_vars.sh

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
        # linux. OFI: find bash method of determining arm vs x86 architecture
    cp rabbit_broker_arm_template.yml world_rmq.yml
elif [[ "$OSTYPE" == "darwin"* ]]; then
        # Mac OSX
    cp rabbit_broker_x86_template.yml world_rmq.yml
fi

sed -i "s/UNIVERSE_SHORT_TAG_PLACEHOLDER/$UNIVERSE_SHORT_TAG/g"  world_rmq.yml
sed -i "s/WORLD_RABBIT_FIRST_DOMAIN_NAME_TERM_PLACEHOLDER/$WORLD_RABBIT_FIRST_DOMAIN_NAME_TERM/g"  world_rmq.yml
sed -i "s/WORLD_TYPE_PLACEHOLDER/$WORLD_TYPE/g"  world_rmq.yml
sed -i "s/RABBIT_USER_PLACEHOLDER/$WORLD_RABBIT_USER/g" world_rmq.yml
sed -i  "s/RABBIT_USER_PASSWORD_PLACEHOLDER/$WORLD_RABBIT_PASSWORD/g"  world_rmq.yml

./build_rabbitmq_conf.sh
./build_vhost_env.sh
./build_rabbit_definitions_json.sh

rm build_rabbitmq_conf.sh
rm build_rabbit_definitions_json.sh