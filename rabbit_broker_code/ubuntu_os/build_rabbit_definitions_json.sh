#!/bin/bash
# Run this when bringing up rabbit brokers

source universe_config_vars.sh
source world_config_vars.sh
source world_secret_vars.sh


echo Building rabbit_definitions.json for $WORLD_RABBIT_VHOST

cp rabbitconfig/"$WORLD_TYPE"_rabbit_definitions_template.json rabbitconfig/rabbit_definitions.json
sed -i  "s/WORLD_RABBIT_VHOST_PLACEHOLDER/$WORLD_RABBIT_VHOST/g"  rabbitconfig/rabbit_definitions.json
sed -i  "s/WORLD_RABBIT_USER_PLACEHOLDER/$WORLD_RABBIT_USER/g"  rabbitconfig/rabbit_definitions.json

