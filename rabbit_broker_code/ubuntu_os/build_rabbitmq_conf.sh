#!/bin/bash
# Run this when bringing up rabbit brokers

source universe_config_vars.sh
source world_config_vars.sh
source world_secret_vars.sh


echo Building rabbitmq.conf

cp rabbitconfig/rabbitmq_template.conf rabbitconfig/rabbitmq.conf
sed -i  "s/RABBIT_VHOST_PLACEHOLDER/$WORLD_RABBIT_VHOST/g"  rabbitconfig/rabbitmq.conf
sed -i  "s/RABBIT_USER_PLACEHOLDER/$WORLD_RABBIT_USER/g"  rabbitconfig/rabbitmq.conf
sed -i  "s/RABBIT_USER_PASSWORD_PLACEHOLDER/$WORLD_RABBIT_PASSWORD/g"  rabbitconfig/rabbitmq.conf





